import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

import static org.testng.Assert.assertEquals;

public class JavaScripAlertsTests {
    private WebDriver driver;

    @BeforeMethod
    public void beforeTest() {
        System.setProperty("webdriver.chrome.driver", "C:/Projects/SeleniumWebdriverJava/JavaTestSelenium_JavaStart/resources/chromedriver.exe");

        driver = new ChromeDriver();
        driver.navigate().to("http://theinternet.przyklady.javastart.pl/javascript_alerts");
    }

    @Test
    public void javaScriptAlertTest() {
        WebElement jsAlertButton = driver.findElement(By.cssSelector("button[onclick='jsAlert()']"));
        jsAlertButton.click();

        driver.switchTo().alert().accept();
        String text = driver.findElement(By.id("result")).getText();
        assertEquals(text, "You successfuly clicked an alert");
    }

    @Test
    public void javaScriptAlertConfirmTest() {
        WebElement jsConfirmButton = driver.findElement(By.cssSelector("button[onclick='jsConfirm()']"));
        jsConfirmButton.click();

        driver.switchTo().alert().accept();
        WebElement result = driver.findElement(By.id("result"));
        assertEquals(result.getText(), "You clicked: Ok");

        jsConfirmButton.click();
        driver.switchTo().alert().dismiss();
        assertEquals(result.getText(), "You clicked: Cancel");
    }

    @Test
    public void javaScriptAlertPromptTest() {
        WebElement jsConfirmButton = driver.findElement(By.cssSelector("button[onclick='jsPrompt()']"));
        jsConfirmButton.click();


/*        String typedText = "Selenium is cool";
        Alert alert = driver.switchTo().alert();
        alert.sendKeys(typedText);
        alert.accept();*/

        driver.switchTo().alert().sendKeys("Selenium is cool");
        driver.switchTo().alert().accept();
        WebElement result = driver.findElement(By.id("result"));
        assertEquals(result.getText(), "You entered: Selenium is cool");

    }


    @AfterMethod
    public void afterTest() {
        driver.quit();
    }

}
