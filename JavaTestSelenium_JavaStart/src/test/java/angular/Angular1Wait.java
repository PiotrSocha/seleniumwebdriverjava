package angular;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class Angular1Wait {

    public static ExpectedCondition<Boolean> waitForAngularToFinishProcessingRequests() {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                String result = executor.executeScript("" +
                        "if (window.angular === undefined) " + // Sprawdzamy, czy aplikacja AngularJS jest dostępna
                        "{return false} " + // Jeśli nie kod JS zwróci fałsz
                        "else{ " +
                        "var injector = window.angular.element('body').injector();" + //Jeśli AngularJS jest dostępny tworzymy obiekt injectora, w którym zaszyte jest pole pendingRequests
                        "return injector.get('$http').pendingRequests.length === 0" + //Sprawdzamy czy wartośc pola jest równa zero, jeśli tak to kod zwróci prawdę.
                        "}").toString();
                return Boolean.valueOf(result);
            }
        };
    }
}


// Wywołanie metody
/*
        driver.navigate().to("https://angularjs.org/");
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(Angular1Wait.waitForAngularToFinishProcessingRequests());*/
