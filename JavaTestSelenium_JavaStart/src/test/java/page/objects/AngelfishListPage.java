package page.objects;

import driver.manager.DriverManager;
import waits.WaitForElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AngelfishListPage extends BasePage {

    @FindBy(css = "a.Button[href$='EST-2']")
    private WebElement smallAngelfishAddToCartButton;


    public ShoppingCartPage clickOnAddToCartSmallAngelfish() {
        WaitForElement.waitUntilElementIsVisible(smallAngelfishAddToCartButton);
        smallAngelfishAddToCartButton.click();
        log().info("Clicked on Small Angelfish Add to cart button");
        return new ShoppingCartPage();
    }
}
