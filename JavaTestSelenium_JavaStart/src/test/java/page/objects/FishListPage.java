package page.objects;

import driver.manager.DriverManager;
import waits.WaitForElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FishListPage extends BasePage {

    @FindBy(css = "tr:nth-child(2) a")
    private WebElement angelfishIdLink;


    public AngelfishListPage clickOnAngelfishId() {
        WaitForElement.waitUntilElementIsVisible(angelfishIdLink);
        angelfishIdLink.click();
        log().info("Clicked on Angelfish Link");
        return new AngelfishListPage();
    }

}
