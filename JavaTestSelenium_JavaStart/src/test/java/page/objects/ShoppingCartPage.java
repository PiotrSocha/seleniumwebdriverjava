package page.objects;

import driver.manager.DriverManager;
import io.qameta.allure.internal.shadowed.jackson.databind.ser.Serializers;
import waits.WaitForElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCartPage extends BasePage {

    @FindBy(css = "a[href$='newOrderForm=']")
    private WebElement proceedToCheckoutButton;


    public CheckoutPage clickOnProceedToCheckout() {
        WaitForElement.waitUntilElementIsVisible(proceedToCheckoutButton);
        proceedToCheckoutButton.click();
        log().info("Clicked on Checkout Button");
        return new CheckoutPage();

    }
}
